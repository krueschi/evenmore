# Evenmore Theme

The evenmore_theme is a Drupal 7.x theme and build as a subtheme of the contributed theme [responsive_bartik](https://www.drupal.org/project/responsive_bartik).

This theme is required as a base theme in your project. It is not required to enable it.
